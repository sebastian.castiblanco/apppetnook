import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule'},
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard] },
  { path: 'camaras', loadChildren: './pages/camaras/camaras.module#CamarasPageModule', canActivate: [AuthGuard] },
  { path: 'detalle-producto/:id', 
  loadChildren: './pages/detalle-producto/detalle-producto.module#DetalleProductoPageModule', canActivate: [AuthGuard]},
  { path: 'slides-camara', loadChildren: './pages/slides-camara/slides-camara.module#SlidesCamaraPageModule', canActivate: [AuthGuard] }
];

// canActivate: [AuthGuard]
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
