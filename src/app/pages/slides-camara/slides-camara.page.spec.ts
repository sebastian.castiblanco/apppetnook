import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidesCamaraPage } from './slides-camara.page';

describe('SlidesCamaraPage', () => {
  let component: SlidesCamaraPage;
  let fixture: ComponentFixture<SlidesCamaraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidesCamaraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidesCamaraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
