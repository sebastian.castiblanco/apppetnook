import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-slides-camara',
  templateUrl: './slides-camara.page.html',
  styleUrls: ['./slides-camara.page.scss'],
})
export class SlidesCamaraPage implements OnInit {

  slides: { img: string, titulo: string, desc: string }[] = [
    {
      img: '/assets/slides/clock-1.svg',
      titulo: 'Pet Nook',
      desc: 'Es consciente que muchas veces nuestros clientes no disponen de suficiente tiempo para sus mascotas, es por eso que se busca brindar un servicio de 24h.'
    },
    {
      img: '/assets/slides/photo-camera-1.svg',
      titulo: 'Pet Nook',
      desc: 'Busca que el negocio de las mascotas sea innovador, es por eso que se cuenta con un sistema de seguridad integral.'
    },
    {
      img: '/assets/slides/smartphone-7.svg',
      titulo: 'Pet Nook',
      desc: 'Gracias a nuestra aplicación móvil, nuestros clientes podrán acceder a las cámaras de seguridad para asegurar el bienestar de sus mascotas.'
    },
    
  ];

  ocultar = '';

  constructor( private navCtrl: NavController ) { }
  
  ngOnInit() {
  }

  onClick() {
    this.ocultar = 'animated fadeOut fast';
    this.navCtrl.navigateBack('/camaras');
  }
}
