import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  slides: { img: string }[] = [
    {
      img: '/assets/slide1.jpeg',
    },
    {
      img: '/assets/slide2.jpeg',
    }
  ];

  
  productos: Observable<any>;

  constructor(
    public menuCtrl: MenuController,
    private dataService: DataService) {
    this.menuCtrl.enable(true);
  }
  
  ngOnInit() {
    this.productos = this.dataService.getProductsLits();
  }

}
