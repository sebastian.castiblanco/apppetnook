import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPagosPage } from './modal-pagos.page';

describe('ModalPagosPage', () => {
  let component: ModalPagosPage;
  let fixture: ComponentFixture<ModalPagosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPagosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPagosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
