import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-modal-pagos',
  templateUrl: './modal-pagos.page.html',
  styleUrls: ['./modal-pagos.page.scss'],
})
export class ModalPagosPage implements OnInit {

  ciudades: Observable<any>;
    
  constructor(private modalCtr : ModalController, private dataService : DataService) { }

  ngOnInit() {
    this.ciudades = this.dataService.getCiudades();
  }


  cerrarModal(){
    this.modalCtr.dismiss();
  }

}
