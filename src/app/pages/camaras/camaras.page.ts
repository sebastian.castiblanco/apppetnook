import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-camaras',
  templateUrl: './camaras.page.html',
  styleUrls: ['./camaras.page.scss'],
})
export class CamarasPage implements OnInit {

  camaras: { img: string, nombre: string, zona: string}[] = [
    {
      img: '/assets/slide1.jpeg',
      nombre: 'Camara una',
      zona: 'zona 1',
    },
    {
      img: '/assets/slide1.jpeg',
      nombre: 'Camara una',
      zona: 'zona 1',
    },
    {
      img: '/assets/slide1.jpeg',
      nombre: 'Camara una',
      zona: 'zona 1',
    },
    {
      img: '/assets/slide1.jpeg',
      nombre: 'Camara una',
      zona: 'zona 1',
    },
    {
      img: '/assets/slide1.jpeg',
      nombre: 'Camara una',
      zona: 'zona 1',
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
