import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import {AuthService} from './../../services/auth.service';
import {User} from './../../interfaces/interaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: User = new User();

  constructor(public menuCtrl: MenuController, private authSvc: AuthService, private router: Router) {
    this.menuCtrl.enable(false);
   }

  ngOnInit() {
  }

  async onLogin(){
    const user = await this.authSvc.onLogin(this.user);
    if(user){
      this.router.navigateByUrl('/home');
    }
  }

}
