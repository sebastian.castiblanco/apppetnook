import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalleProductoPage } from './detalle-producto.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ModalPagosPage } from '../modal-pagos/modal-pagos.page';
import { ModalPagosPageModule } from '../modal-pagos/modal-pagos.module';

const routes: Routes = [
  {
    path: '',
    component: DetalleProductoPage
  }
];

@NgModule({
  entryComponents: [
      ModalPagosPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ModalPagosPageModule
  ],
  declarations: [DetalleProductoPage]
})
export class DetalleProductoPageModule {}
