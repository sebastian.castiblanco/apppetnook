import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { ModalPagosPage } from '../modal-pagos/modal-pagos.page';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.page.html',
  styleUrls: ['./detalle-producto.page.scss'],
})
export class DetalleProductoPage implements OnInit {

  detalle: Observable<any>;

  private id: string;


  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private modalCtr: ModalController) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.detalle = this.dataService.getProductsLitsDetails(this.id);
  }

  async abrirModal() {
    const modal = await this.modalCtr.create(
      {
        component: ModalPagosPage,
        componentProps: {
          'firstName': 'Douglas',
          'lastName': 'Adams',
          'middleInitial': 'N'
        }
      }
    );
    await modal.present();
  }

}
