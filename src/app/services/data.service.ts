import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Componente } from '../interfaces/interaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getMenuOpts() {
    return this.http.get<Componente[]>("/assets/data/menu.json");
  }

  getProductsLits() {
    return this.http.get('http://sebastiancastiblanco.com/api/getListProducts.php');
  }

  getProductsLitsDetails(id) {
    return this.http.get('http://sebastiancastiblanco.com/api/getListProductsDetail.php?id='+id+'');
  }

  getCiudades() {
    return this.http.get<Componente[]>("/assets/data/ciudades.json");
  }

}
