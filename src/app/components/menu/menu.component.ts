import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { Componente } from 'src/app/interfaces/interaces';
import { Router} from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  componentes: Observable<Componente[]>;

  constructor( 
    private dataService: DataService, 
    private authSvc: AuthService, 
    private router: Router, 
    private afAuth: AngularFireAuth
    ) { }


  ngOnInit() {
    this.componentes = this.dataService.getMenuOpts();
  }

  onLogout(){
    console.log('termino');
    this.afAuth.auth.signOut();
    this.router.navigateByUrl('/login');
  }

}
